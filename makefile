CC=gcc
CFLAGS=-Wall -g
LDFLAGS=-lfl

bin/tpcc: bin obj obj/abstract-tree.o obj/symbol-table.o obj/to_asm_file.o obj/parser.tab.o obj/lex.yy.o 
	$(CC) -o bin/tpcc obj/lex.yy.o obj/parser.tab.o obj/abstract-tree.o obj/symbol-table.o obj/to_asm_file.o $(LDFLAGS)

bin:
	mkdir bin

obj:
	mkdir obj

obj/lex.yy.o: src/lex.yy.c src/parser.tab.h
	$(CC) -o obj/lex.yy.o -c src/lex.yy.c $(CFLAGS)

obj/parser.tab.o: src/parser.tab.c src/parser.tab.h src/abstract-tree.h src/symbol-table.h src/to_asm_file.h
	$(CC) -o obj/parser.tab.o -c src/parser.tab.c $(CFLAGS)

src/parser.tab.c src/parser.tab.h: src/parser.y src/abstract-tree.h src/symbol-table.h src/to_asm_file.h
	bison -d src/parser.y
	mv parser.tab.* -t src

obj/abstract-tree.o: src/abstract-tree.c src/abstract-tree.h
	$(CC) -o obj/abstract-tree.o -c src/abstract-tree.c $(CFLAGS)

obj/symbol-table.o: src/symbol-table.c src/abstract-tree.h src/symbol-table.h
	$(CC) -o obj/symbol-table.o -c src/symbol-table.c $(CFLAGS)

obj/to_asm_file.o: src/to_asm_file.c src/abstract-tree.h src/symbol-table.h src/to_asm_file.h
	$(CC) -o obj/to_asm_file.o -c src/to_asm_file.c $(CFLAGS)

src/lex.yy.c: src/lexer.lex
	flex src/lexer.lex
	mv lex.yy.c -t src/


test: script.sh
	sh script.sh


clean:
	rm -f src/parser.tab.* src/lex.yy.c
	rm -Rf obj

mrproper: clean
	rm -Rf bin
	rm -f testfile

