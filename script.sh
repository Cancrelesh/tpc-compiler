#! /bin/bash
for dir in $(ls ./test)
  do
    echo -e "\n\tDossier : $dir\n"
    for file in $(ls ./test/$dir) #Parcour tous les fichiers du repertoire test/ et utilise le compilateur tpcc dessus.
      do
        echo -e "Fichier : $file"
        ./bin/tpcc < ./test/$dir/$file
        retour=$?
        if [ $retour -eq 0 ] #Test si le code retour de la commande precedante vaut 0 (le fichier n'a aucun probleme)
          then echo -e "Le fichier $file est syntaxiquement correct (code de retour : $retour)\n\n"
        elif [ $retour -eq 1 ]  #Le code retour de la commande vaut 1 (le fichier a un probleme)
          then echo -e "Le fichier $file est syntaxiquement incorrect (code de retour : $retour)\n\n"
        elif [ $retour -eq 2 ]  #Le code retour de la commande vaut 2 (le fichier a un probleme)
          then echo -e "Le fichier $file est sémantiquement incorrect (code de retour : $retour)\n\n"
        else  #Le code retour de la commande vaut 3 (le fichier a un probleme)
          echo -e "Le fichier $file possède une erreur (code de retour : $retour)\n"
        fi
      done
  done

