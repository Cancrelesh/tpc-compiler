# TPC-Compiler

The goal of this project is the creation of a lexical analyzer/compiler of .tpc ("very little C" or "très petit C" in french) script with flex and bison.

src/ contains files sources of the project.
test/ contains .tpc scripts which are store in differents subdirectory good/ sem-err/ syn-err/ warn/.
test/good/ contains .tpc which compiles without any problem.
test/sem-err/ contains .tpc which triggers semantique errors.
test/syn-err/ contains .tpc which triggers syntaxique errors.
test/warn/ contains .tpc which triggers warnings.

First use the makefile with the command ```make``` to generate the compiler tpcc which is in the bin/ folder.
For use the compiler enter the command ```bin/tpcc < test/subdirectory/fileyouwant.tpc```. This command generate the file bin/_anonymous.asm (name by default).
Then the command ```nasm -f elf64 bin/_anonymous.asm -o bin/_anonymous.o``` and ```gcc -o testfile bin/_anonymous.o -nostartfiles -no-pie``` to generate the executable testfile.
And use ```./testfile``` to execute.

If you want to use the compiler on all files wich are in test/ you can use script.sh with the command ```make test``` but for the moment this script don't generate the executables.

You can use ```make clean``` to clean the project and ```make mrproper``` which clean the project too and delete /bin and the executable testfile.

When the compiler return 0, there is no error in the .tpc file.
When the compiler return 1, there is a syntaxique error in the .tpc file.
When the compiler return 2, there is a semantique error in the .tpc file.
The compiler return 3 for the errors that are not handle above.