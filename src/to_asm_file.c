#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "abstract-tree.h"
#include "symbol-table.h"
#include "to_asm_file.h"

/* to_asm_file.c */
/* 
compiler en assembleur :
nasm -f elf64 file.asm -> file.o
gcc -o testfile file.o -nostartfiles -no-pie -> testfile
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "abstract-tree.h"
#include "symbol-table.h"
#include "to_asm_file.h"

Datas* initDatas() {

    Datas* datas = NULL;
    datas = (Datas*) malloc(sizeof(Datas));
    strcpy(datas->name, "");
    datas->type = u;
    datas->next = NULL;
    return datas;
}

void removeDatas(Datas* datas) {

    if(datas->next != NULL) {

        removeDatas(datas->next);
    }

    free(datas);
    datas = NULL;
}

int insertDatas(Datas** datas, char* tableName, char* var, char* type) {

    char var_name[64] = "";
    
    if(*datas == NULL) {
        
        *datas = initDatas();
        if(*datas == NULL) {
            return 0;
        }
    }
    if(!strcmp((*datas)->name, "")) {
        
        
        strcat(var_name, tableName);
        strcat(var_name, "_");
        strcat(var_name, var);
        strcpy((*datas)->name, var_name);

        if(!strcmp(type, "int")) {
            (*datas)->type = i;
        
        } else {
            (*datas)->type = c;
        }
        return 1;
    
    }
    return insertDatas(&(*datas)->next, tableName, var, type);
}

int addDatasValue(Datas** datas, char* tableName, char* var, Node* value) {
    
    char find_data[64] = "";
    
    strcat(find_data, tableName);
    strcat(find_data, "_");
    strcat(find_data, var);
    
    if(*datas == NULL) {

        return 0;
    }
    
    if(!strcmp((*datas)->name, find_data)){
        
        if((*datas)->type == i) {
            (*datas)->val.i = value->u.integer;
        
        } else {

            (*datas)->val.c = value->u.character;
        }

        return 1;
    }

    return addDatasValue(&(*datas)->next, tableName, var, value);   
}

void printDatas(Datas* datas) {

    while(datas != NULL) {

        if(datas->type == i) {
            printf("int %s = %d\n", datas->name, datas->val.i);
        
        } else if(datas->type == c){
            printf("char %s = %c\n", datas->name, datas->val.c);
        }

        datas = datas->next;
    }

    printf("fin datas\n");
}

void printDatasASM(Datas* datas, FILE* output) {

    while(datas != NULL) {

        if(datas->type == i) {
            fprintf(output, "%s: dq\t%d\n", datas->name, datas->val.i);
        
        } else if(datas->type == c){
            fprintf(output, "%s: db\t\'%c\'\n", datas->name, datas->val.c);
        }

        datas = datas->next;
    }
}

int writeAsmByReaddingTree(FILE* output, Node* tree, SymbolTable* st, Datas* datas) {
    
    char var[64] = "";
    LineTable* line = NULL;
    SymbolTable* stCpy = NULL;
    Datas* cpDatas = NULL;

    switch(tree->kind) {
        
        case Prog: 
            fprintf(output,"global\t_start\n");
            fprintf(output, "extern\tprintf\n\n");
            /*writeAsmByReaddingTree(output, FIRSTCHILD(tree), st);*/ /* TypeVars */
            
            fprintf(output,"section\t.text\n");
            writeAsmByReaddingTree(output, SECONDCHILD(tree), st, datas); /* DeclFoncts */
            /*printDatas(datas);*/
            
            fprintf(output, "\nsection\t.data\n");
            printDatasASM(datas, output);
            fprintf(output, "print_int: db '%%d',10,0\nprint_char: db '%%c',10,0\n"); /* data to print int and char */
            break;
        
        case VarDeclList: break;
        case TypesVars: break;
        case FunDeclList: break;

        case DeclFoncts: 
            if(FIRSTCHILD(tree)->kind != DeclFonct) {
                writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas); /* DeclFoncts */
                writeAsmByReaddingTree(output, SECONDCHILD(tree), st, datas); /* DeclFonct */
            
            } else {
                writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas); /* DeclFonct */
            }
            break;

        case Type: break;
        case Declarateurs: break;
        case Semicolon: break;
        case Struct: break;
        case Ident: break;
        case OpenBrace: break;
        case DeclChamps: break;
        case CloseBrace: break;
        case Simpletype: break;
        case Comma: break;
        
        case DeclFonct:
            stCpy = st;
            writeAsmByReaddingTree(output, FIRSTCHILD(tree), stCpy->nextTable, datas); /* EnTeteFonct */
            while(strcmp(stCpy->tableName, FIRSTCHILD(tree)->firstChild->nextSibling->u.identifier)) {
                stCpy = stCpy->nextTable;

            } /* On vérifie que l'on a bien la table de la bonne fonction */
            writeAsmByReaddingTree(output, SECONDCHILD(tree), stCpy, datas); /* Corps */
            break;
        
        case EnTeteFonct:
            if(!strcmp(st->tableName, "main")) {
                fprintf(output, "_start:\n"); /* main */
            }
            break;

        case Corps: 
            writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas); /* DeclVars */
            writeAsmByReaddingTree(output, SECONDCHILD(tree), st, datas); /* SuiteInstr */
            break;

        case OpenBracket: break;
        case Parametres: break;
        case CloseBracket: break;
        case Void: break;
        case ListTypVar: break;
        
        case DeclVars: 
            /* On remplit la structure datas */
            line = st->firstLine;
            while (line != NULL) {
                insertDatas(&datas, st->tableName, line->var, line->type);
                line = line->nextLine;
            }
            break;

        case SuiteInstr: 
            if(FIRSTCHILD(tree) != NULL) {
                writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas); /* SuiteInstr */
                writeAsmByReaddingTree(output, SECONDCHILD(tree), st, datas); /* Instr */   
            } 
            break;

        case Instr: 
            if(FIRSTCHILD(tree)->kind == Return) { /* Return */        
                writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas);
            }
            if(FIRSTCHILD(tree)->kind == Lvalue) { /* Lvalue = */
                writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas);
            }
            if(FIRSTCHILD(tree)->kind == Print) { /* Print */
                writeAsmByReaddingTree(output, FIRSTCHILD(tree), st, datas);
            }
            break;

        case Lvalue:
            if(tree->nextSibling != NULL) { /* Lvalue = Exp */
                if(tree->nextSibling->nextSibling->kind == Num || tree->nextSibling->nextSibling->kind == Character) { /* Num or Character */
                    addDatasValue(&datas, st->tableName, tree->firstChild->u.identifier, tree->nextSibling->nextSibling);
                }
                if(tree->nextSibling->nextSibling->kind == AddSub) { /* int1 + int2 */
                    strcat(var, st->tableName);
                    strcat(var, "_");
                    strcat(var, FIRSTCHILD(tree)->u.identifier);
                    fprintf(output, "mov rax, ");
                    writeAsmByReaddingTree(output, tree->nextSibling->nextSibling, st, datas);
                    fprintf(output, "mov [%s], rax\n", var);
                }
            } else { /* call Lvalue */
                strcat(var, st->tableName);
                strcat(var, "_");
                strcat(var, FIRSTCHILD(tree)->u.identifier);
                cpDatas = datas;
                while(cpDatas != NULL && strcmp(cpDatas->name, var)) {
                    cpDatas = cpDatas->next;
                }
                fprintf(output,"[%s]\n", cpDatas->name);
            }
            break;

        case Equal: break;
        case Exp: break;
        case Reade: break;
        case Readc: break;
        
        case Print: 
            if(tree->nextSibling->kind == Num) {
                fprintf(output, "mov rax, %d\n", tree->nextSibling->u.integer);
                fprintf(output, "mov rdi, print_int\n");
                
            }
            if(tree->nextSibling->kind == Lvalue) {
                strcat(var, st->tableName);
                strcat(var, "_");
                strcat(var, tree->nextSibling->firstChild->u.identifier);
                fprintf(output, "mov rax, [%s]\n", var);
                cpDatas = datas;
                while(cpDatas != NULL && strcmp(cpDatas->name, var)) {
                    cpDatas = cpDatas->next;
                }
                if(cpDatas->type == i) {
                    fprintf(output, "mov rdi, print_int\n");
                }
                if(cpDatas->type == c) {
                    fprintf(output, "mov rdi, print_char\n");
                }
            }
            fprintf(output, "mov rsi, rax\n");
            fprintf(output, "call printf\n");
            break;

        case If: break;
        case Else: break;
        case While: break;
        
        case Return: 
            if(tree->nextSibling != NULL) {
                if(!strcmp(st->tableName, "main")) { /* Fonction main */
                    fprintf(output,"mov rax, 1\nmov rbx, ");
                    writeAsmByReaddingTree(output, tree->nextSibling, st, datas); /* Exp */
                    fprintf(output,"\nint 0x80\n");
                }
            }
            break;

        case Or: break;
        case Tb: break;
        case And: break;
        case Fb: break;
        case Eq: break;
        case m: break;
        case Order: break;
        case e: break;
        
        case AddSub:
            if(FIRSTCHILD(tree)->kind == Lvalue) {
                strcat(var, st->tableName);
                strcat(var, "_");
                strcat(var, FIRSTCHILD(tree)->firstChild->u.identifier);
                fprintf(output, "[%s]\n", var);
            
            } else if(FIRSTCHILD(tree)->kind == Num) {
                fprintf(output, "%d\n", FIRSTCHILD(tree)->u.integer);
            }

            if(SECONDCHILD(tree)->u.character == '+') {
                fprintf(output, "add rax,");

            } else if(SECONDCHILD(tree)->u.character == '-') {
                fprintf(output, "sub rax,");
            }

            if(THIRDCHILD(tree)->kind == Lvalue) {
                strcpy(var, "");
                strcat(var, st->tableName);
                strcat(var, "_");
                strcat(var, THIRDCHILD(tree)->firstChild->u.identifier);
                fprintf(output, " [%s]\n", var);
            
            } else if (THIRDCHILD(tree)->kind == Num) {
                fprintf(output, " %d\n", THIRDCHILD(tree)->u.integer);
            }
            break;
        
        case t: break;
        case DivStar: break;
        case Not: break;
        case Ampersand: break;
        
        case Num: 
            fprintf(output,"%d", tree->u.integer);
            break;
        
        case Character: 
            fprintf(output,"%c", tree->u.character);
            break;

        case Arguments: break;
        case Point: break;
        case ListExp: break;
        case f: break;
        default: break;
    }
    return 1;
}

int createFileAsm(Node* tree, SymbolTable* st) {

    char* filename = "./bin/_anonymous.asm";
    FILE* output = NULL;
    Datas* datas = NULL;
    output = fopen(filename, "w");
    datas = initDatas();
    writeAsmByReaddingTree(output, tree, st, datas);    
    fclose(output);
    return 1;
}

