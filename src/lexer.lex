%{
  /* lexer.l */
  /* Compatible avec parser.y */
  #define MAXNAME 30
  #include "abstract-tree.h"
  #include "symbol-table.h"
  #include "parser.tab.h"
  int lineno = 1;
  int charno = 0;
  char* err_line;
%}

%option noinput nounput noyywrap
%x SHORT_COMMENT LONG_COMMENT

%%
[ \t\r]+		                    { charno += yyleng; }
\n			                        { charno = 0; lineno++; }
"/*"			                      { charno += yyleng; BEGIN LONG_COMMENT; }
"//"			                      { charno += yyleng; BEGIN SHORT_COMMENT; }
^.*\n									          { err_line = (char*) malloc(sizeof(char) * (yyleng + 1)); strcpy(err_line, yytext); REJECT; }
&&			                        { charno += yyleng; return AND; }
"||"			                      { charno += yyleng; return OR; }
"+"|-			                      { charno += yyleng; yylval.addsub = yytext[0]; return ADDSUB; }
"*"|"/"|"%"			                { charno += yyleng; yylval.divstar = yytext[0]; return DIVSTAR; }
"<"|"<="|">"|>=		              { charno += yyleng; strcpy(yylval.check, yytext); return ORDER; }
==|!=			                      { charno += yyleng; strcpy(yylval.check, yytext); return EQ; }
int			                        { charno += yyleng; strcpy(yylval.simpletype, yytext); return SIMPLETYPE; }
char		                        { charno += yyleng; strcpy(yylval.simpletype, yytext);  return SIMPLETYPE; }
print			                      { charno += yyleng; return PRINT; }
readc			                      { charno += yyleng; return READC; }
reade			                      { charno += yyleng; return READE; }
void			                      { charno += yyleng; return VOID; }
struct		                      { charno += yyleng; return STRUCT; }
if			                        { charno += yyleng; return IF; }
else			                      { charno += yyleng; return ELSE; }
while			                      { charno += yyleng; return WHILE; }
return			                    { charno += yyleng; return RETURN; }
[a-zA-Z_][a-zA-Z0-9_]*	        { charno += yyleng; strcpy(yylval.ident, yytext); return IDENT; }
[0-9]+			                    { charno += yyleng; yylval.num = atoi(yytext); return NUM; }
'\\?.'			                    { charno += yyleng; yylval.c = yytext[1]; return CHARACTER; }
.			                          { charno += yyleng; return yytext[0]; }
<LONG_COMMENT>"*/"		          { BEGIN INITIAL; charno += yyleng; }
<LONG_COMMENT,SHORT_COMMENT>.		{ charno += yyleng; }
<LONG_COMMENT>\n		            { lineno++; charno = 0; }
<SHORT_COMMENT>\n		            { BEGIN INITIAL; lineno++; charno = 0; }
%%
