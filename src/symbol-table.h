/* symbol-table.h */
#ifndef TABLE
#define TABLE

typedef struct lineTable {

    char type[64];
    char var[64];
    struct lineTable* nextLine;

} LineTable;

typedef struct symbolTable {
    
    char tableName[64];
    struct lineTable* firstLine;
    struct symbolTable* nextTable;
    
} SymbolTable;

SymbolTable* makeNewTable(char* name);
LineTable* makeNewLine();
int Table(LineTable* lt, char* var);

int fillLine(LineTable* lt, char* type, char* var);

int printTables(SymbolTable* st);
int printTable(LineTable* lt);

void deleteTables(SymbolTable* st);
void deleteLines(LineTable* lt);

int addTable(SymbolTable** st, SymbolTable** nt);
int addLine(LineTable** lt, LineTable** nl);

int fillSymbolTable(SymbolTable** st, Node* tree);

char* getType(Node* tree);
int getVars(Node* tree, char vars[64][64], int pos);

#endif