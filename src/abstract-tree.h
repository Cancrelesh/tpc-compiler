/* abstract-tree.h */

#ifndef TREE
#define TREE

typedef enum {
  Prog,
  VarDeclList,
  TypesVars,
  FunDeclList,
  DeclFoncts,
  Type,
  Declarateurs,
  Semicolon,
  Struct,
  Ident,
  OpenBrace,
  DeclChamps,
  CloseBrace,
  Simpletype,
  Comma,
  DeclFonct,
  EnTeteFonct,
  Corps,
  OpenBracket,
  Parametres,
  CloseBracket,
  Void,
  ListTypVar,
  DeclVars,
  SuiteInstr,
  Instr,
  Lvalue,
  Equal,
  Exp,
  Reade,
  Readc,
  Print,
  If,
  Else,
  While,
  Return,
  Or,
  Tb,
  And,
  Fb,
  Eq,
  m,
  Order,
  e,
  AddSub,
  t,
  DivStar,
  Not,
  Ampersand,
  Num,
  Character,
  Arguments,
  Point,
  ListExp,
  f
  /* The list must coincide with the strings in abstract-tree.c */
  /* To avoid listing them twice, see https://stackoverflow.com/a/10966395 */
} Kind;

typedef struct Node {
  Kind kind;
  union {
    int integer;
    char character;
    char identifier[64];
  } u;
  struct Node *firstChild, *nextSibling;
  int lineno;
} Node;

Node *makeNode(Kind kind);
void addSibling(Node *node, Node *sibling);
void addChild(Node *parent, Node *child);
void deleteTree(Node*node);
void printTree(Node *node);

#define FIRSTCHILD(node) node->firstChild
#define SECONDCHILD(node) node->firstChild->nextSibling
#define THIRDCHILD(node) node->firstChild->nextSibling->nextSibling

#endif