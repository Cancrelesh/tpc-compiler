%{
/* parser.y */
/* Syntaxe du TPC pour le projet d'analyse syntaxique de 2020-2021 */
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <getopt.h>
  #include <unistd.h>

  #include "abstract-tree.h"
  #include "symbol-table.h"
  #include "to_asm_file.h"

  extern int lineno;
  extern int charno;
  int yylex();
  void yyerror(const char *);
  int yyparse();
  int parse;
  extern char* err_line;
  int tree, symtabs, help;
  SymbolTable* st = NULL;
  int checkST = 0;
  extern FILE* yyin;

%}
%union {
    int num;
    char c;
    char check[2];
    char ident[64];
    char simpletype[4];
    char addsub;
    char divstar;
    Node* node;
}
%token <c> CHARACTER
%token <num> NUM
%token <ident> IDENT
%token <simpletype> SIMPLETYPE
%token <check> ORDER EQ
%token <addsub> ADDSUB
%token <divstar> DIVSTAR
%token <node> OR AND STRUCT IF WHILE RETURN VOID PRINT READC READE
%precedence ')'
%precedence ELSE
%type <node> '.' ';' ',' '{' '}' '(' ')' '=' '&' '!' Prog TypesVars DeclFoncts Type Declarateurs DeclChamps DeclFonct EnTeteFonct Corps Parametres ListTypVar DeclVars SuiteInstr Instr LValue Exp ELSE F TB FB M E T Arguments ListExp

%%

Prog:  
       TypesVars DeclFoncts                             {$$ = makeNode(Prog); addChild($$, $1); addSibling($1, $2); if(tree){printTree($$);}; st = makeNewTable("GLOBAL"); checkST = fillSymbolTable(&st, $$); if(symtabs){printTables(st);}; if(checkST == 2){deleteTables(st); deleteTree($$); exit(2);}; createFileAsm($$, st); deleteTables(st); deleteTree($$);}
    ;
TypesVars:
       TypesVars Type Declarateurs ';'                  {$$ = makeNode(TypesVars); addChild($$, $1); addSibling($1, $2); addSibling($2, $3);}
    |  TypesVars STRUCT IDENT '{' DeclChamps '}' ';'    {$$ = makeNode(TypesVars); $2 = makeNode(Struct); Node* n = makeNode(Ident); strcpy(n->u.identifier, $3); addChild($$, $1); addSibling($1, $2); addSibling($2, n); addSibling(n, $5);}
    |  %empty                                           {$$ = makeNode(TypesVars);}
    ;
Type:
       SIMPLETYPE                                       {$$ = makeNode(Type); Node* n = makeNode(Simpletype); Node* m = makeNode(Ident); strcpy(m->u.identifier, $1); addChild($$, n); addChild(n, m);}
    |  STRUCT IDENT                                     {$$ = makeNode(Type); $1 = makeNode(Struct); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2); addChild($$, $1); addSibling($1, n);}
    ;
Declarateurs:
       Declarateurs ',' IDENT                           {$$ = makeNode(Declarateurs); Node* n = makeNode(Ident); strcpy(n->u.identifier, $3); addChild($$, $1); addSibling($1, n);}
    |  IDENT                                            {$$ = makeNode(Declarateurs); Node* n = makeNode(Ident); strcpy(n->u.identifier, $1); addChild($$, n);}
    ;
DeclChamps :
       DeclChamps SIMPLETYPE Declarateurs ';'           {$$ = makeNode(DeclChamps); Node* n = makeNode(Simpletype); Node* m = makeNode(Ident); strcpy(m->u.identifier, $2); addChild($$, $1); addSibling($1, n); addChild(n, m); addSibling(n, $3);}
    |  SIMPLETYPE Declarateurs ';'                      {$$ = makeNode(DeclChamps); Node* n = makeNode(Simpletype); Node* m = makeNode(Ident); strcpy(m->u.identifier, $1); addChild($$, n); addChild(n, m); addSibling(n, $2);}
    ;
DeclFoncts:
       DeclFoncts DeclFonct                             {$$ = makeNode(DeclFoncts); addChild($$, $1); addSibling($1, $2);}
    |  DeclFonct                                        {$$ = makeNode(DeclFoncts); addChild($$, $1);}
    ;
DeclFonct:
       EnTeteFonct Corps                                {$$ = makeNode(DeclFonct); addChild($$, $1); addSibling($1, $2);}
    ;
EnTeteFonct:
       Type IDENT '(' Parametres ')'                    {$$ = makeNode(EnTeteFonct); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2); addChild($$, $1); addSibling($1, n); addSibling(n, $4);}
    |  VOID IDENT '(' Parametres ')'                    {$$ = makeNode(EnTeteFonct); $1 = makeNode(Void); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2); addChild($$, $1); addSibling($1, n); addSibling(n, $4);}
    ;
Parametres:
       VOID                                             {$$ = makeNode(Parametres); $1 = makeNode(Void); addChild($$, $1);}
    |  ListTypVar                                       {$$ = makeNode(Parametres); addChild($$, $1);}
    ;
ListTypVar:
       ListTypVar ',' Type IDENT                        {$$ = makeNode(ListTypVar); Node* n = makeNode(Ident); strcpy(n->u.identifier, $4); addChild($$, $1); addSibling($1, $3); addSibling($3, n);}
    |  Type IDENT                                       {$$ = makeNode(ListTypVar); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2); addChild($$, $1); addSibling($1, n);}
    ;
Corps: '{' DeclVars SuiteInstr '}'                      {$$ = makeNode(Corps); addChild($$, $2); addSibling($2, $3);}
    ;
DeclVars:
       DeclVars Type Declarateurs ';'                   {$$ = makeNode(DeclVars); addChild($$, $1); addSibling($1, $2); addSibling($2, $3);}
    |  %empty                                           {$$ = makeNode(DeclVars);}
    ;
SuiteInstr:
       SuiteInstr Instr                                 {$$ = makeNode(SuiteInstr); addChild($$, $1); addSibling($1, $2);}
    |  %empty                                           {$$ = makeNode(SuiteInstr);}
    ;
Instr:
       LValue '=' Exp ';'                               {$$ = makeNode(Instr); $2 = makeNode(Equal); addChild($$, $1); addSibling($1, $2); addSibling($2, $3);}
    |  READE '(' LValue ')' ';'                         {$$ = makeNode(Instr); $1 = makeNode(Reade); addChild($$, $1); addSibling($1, $3);}
    |  READC '(' LValue ')' ';'                         {$$ = makeNode(Instr); $1 = makeNode(Readc); addChild($$, $1); addSibling($1, $3);}
    |  PRINT '(' Exp ')' ';'                            {$$ = makeNode(Instr); $1 = makeNode(Print); addChild($$, $1); addSibling($1, $3);}
    |  IF '(' Exp ')' Instr                             {$$ = makeNode(Instr); $1 = makeNode(If); addChild($$, $1); addSibling($1, $3); addSibling($3, $5);}
    |  IF '(' Exp ')' Instr ELSE Instr                  {$$ = makeNode(Instr); $1 = makeNode(If); $6 = makeNode(Else); addChild($$, $1); addSibling($1, $3); addSibling($3, $5); addSibling($5, $6); addSibling($6, $7);}
    |  WHILE '(' Exp ')' Instr                          {$$ = makeNode(Instr); $1 = makeNode(While); addChild($$, $1); addSibling($1, $3); addSibling($3, $5);}
    |  Exp ';'                                          {$$ = makeNode(Instr); addChild($$, $1);}
    |  RETURN Exp ';'                                   {$$ = makeNode(Instr); $1 = makeNode(Return); addChild($$, $1); addSibling($1, $2);}
    |  RETURN ';'                                       {$$ = makeNode(Instr); $1 = makeNode(Return); addChild($$, $1);}
    |  '{' SuiteInstr '}'                               {$$ = makeNode(Instr); addChild($$, $2);}
    |  ';'                                              
    ;
Exp :  Exp OR TB                                        {$$ = makeNode(Exp); $2 = makeNode(Or); addChild($$, $2); addChild($2, $1); addSibling($1, $3);}
    |  TB                                               {$$ = $1;}
    ;
TB  :  TB AND FB                                        {$$ = makeNode(And); addChild($$, $1); addSibling($1, $3);}
    |  FB                                               {$$ = $1;}
    ;
FB  :  FB EQ M                                          {$$ = makeNode(Eq); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2); addChild($$, $1); addSibling($1, n); addSibling(n, $3);}
    |  M                                                {$$ = $1;}
    ;
M   :  M ORDER E                                        {$$ = makeNode(Order); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2);; addChild($$, $1); addSibling($1, n); addSibling(n, $3);}
    |  E                                                {$$ = $1;}
    ;
E   :  E ADDSUB T                                       {$$ = makeNode(AddSub); Node* n = makeNode(Character); n->u.character = $2; addChild($$, $1); addSibling($1, n); addSibling(n, $3);}
    |  T                                                {$$ = $1;}
    ;
T   :  T DIVSTAR F                                      {$$ = makeNode(DivStar); Node* n = makeNode(Character); n->u.character = $2; addChild($$, $1); addSibling($1, n); addChild(n, $3);}
    |  F                                                {$$ = $1;}
    ;
F   :  ADDSUB F                                         {$$ = makeNode(AddSub); Node* n = makeNode(Character); n->u.character = $1; addChild($$, n); addSibling(n, $2);}
    |  '!' F                                            {$$ = makeNode(Not); addSibling($$, $2);}
    |  '&' IDENT                                        {$$ = makeNode(Ampersand); Node* n = makeNode(Ident); strcpy(n->u.identifier, $2); addSibling($$, n);}
    |  '(' Exp ')'                                      {$$ = $2;}
    |  NUM                                              {$$ = makeNode(Num); $$->u.integer = $1;}
    |  CHARACTER                                        {$$ = makeNode(Character); $$->u.character = $1;}
    |  LValue                                           {$$ =  $1;}
    |  IDENT '(' Arguments ')'                          {$$ = makeNode(Ident); strcpy($$->u.identifier, $1); $3 = makeNode(Arguments); addSibling($$, $3);}
LValue:
       IDENT                                            {$$ = makeNode(Lvalue); Node* n = makeNode(Ident); strcpy(n->u.identifier, $1); addChild($$, n);}
    ;
Arguments:
       ListExp                                          {$$ = makeNode(Arguments); addChild($$, $1);}
    |  %empty                                           {$$ = makeNode(Arguments);}
    ;
ListExp:
       ListExp ',' Exp                                  {$$ = makeNode(ListExp); addChild($$, $1); addSibling($1, $3);}
    |  Exp                                              {$$ = makeNode(ListExp); addChild($$, $1);}
    ;

%%

int main(int argc, char** argv) {

    char c;
    tree = 0;
    symtabs = 0;
    char filename[255];
    FILE* myfile = NULL;

    /* If the last argument is not a file */
    if((myfile = fopen(argv[argc - 1], "r")) == NULL) {
        
        /* check stdin */
        if((fseek(stdin, 0, SEEK_END), ftell(stdin)) > 0) {
            rewind(stdin);
            myfile = stdin;
        }
        /* or ask a file */
        while(myfile == NULL) {

            fprintf(stdout, "Please enter a valid file name (ex: ./waytothefile/file.tpc):\n");
            fscanf(stdin, "%s", &filename);
            printf("filename = %s\n", filename);
            myfile = fopen(filename, "r");
        }
    }

    yyin = myfile;
     
    int print_help(void) {
        
        printf("Use : ./tpcc [OPTION]... [FILE]\n");
        printf("Options :\n");
        printf("-h --help\tShow this help.\n");
        printf("-t --tree\tDisplay the abstract tree of FILE.tpc on the terminal.\n");
        printf("-s --symtabs\tDisplay the symbol table of FILE.tpc on the terminal.\n");
        return 0;  
    }

    static struct option long_options[] = {
        
        {"tree", no_argument, 0, 't'},
        {"symtabs", no_argument, 0, 's'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };

    while((c = getopt_long(argc, argv, "tsh", long_options, 0)) != -1) {
        
        switch(c) {
            
            case 't': 
                tree = 1; break;
            
            case 's': 
                symtabs = 1; break;
            
            case 'h': 
                return print_help();
            
            case '?':
                printf("Use « ./bin/tpcc -h » or « ./bin/tpcc --help » for more informations.\n");
                return 3;
            
            default:
                printf("unknown error when parsing arguments\n");
                return 3;
        }
    }

    return yyparse();
}

void yyerror(const char *s){
	
    fprintf(stderr, "%s near line %d character %d\n%s", s, lineno, charno, err_line);
    free(err_line);
    for(int i = 0; i < charno - 1; i++) {

        fprintf(stderr, " ");
    }
    fprintf(stderr, "^\n");
}
