/* symbol-table.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "abstract-tree.h"
#include "symbol-table.h"

SymbolTable* makeNewTable(char* name) {

    SymbolTable* st;
    st = (SymbolTable*) malloc(sizeof(SymbolTable));
    if(NULL == st) {
        return NULL;
    }

    strcpy(st->tableName, name);
    st->firstLine = NULL;
    st->nextTable = NULL;

    return st; 
}

LineTable* makeNewLine() {

    LineTable* lt;
    lt = (LineTable*) malloc(sizeof(LineTable));
    if(NULL == lt) {
        return NULL;
    }

    lt->nextLine = NULL;
    strcpy(lt->type, "");
    strcpy(lt->var, "");

    return lt; 
}

int isInTable(LineTable* lt, char* var) {

    if(lt == NULL) {
        return 1;
    }
    
    if (strcmp(lt->var, var) == 0) {
        return 0;
    }

    return isInTable(lt->nextLine, var);    
}

int fillLine(LineTable* lt, char* type, char* var) {

    if (lt == NULL) {
        return 0;
    }

    strcpy(lt->type, type);
    strcpy(lt->var, var);

    return 1;
}

int printTables(SymbolTable* st) {

    if (st == NULL) {
        return 0;
    
    } else if (st != NULL) {
        printf("-----------------------\n");
        printf("%s\n", st->tableName);
        printTable(st->firstLine);
        printf("-----------------------\n");
    }

    if (st->nextTable != NULL) {
        printf("\n\n");
        printTables(st->nextTable);
    }

    return 1;
}

int printTable(LineTable* lt) {

    if (lt == NULL) {
        printf("-----------------------\n");
        printf("Empty\n");
        return 0;

    } else if (lt != NULL) {
        printf("-----------------------\n");
        printf("%s : %s\n", lt->type, lt->var);
    }

    if (lt->nextLine != NULL) {
        printTable(lt->nextLine);
    }

    return 1;
}

void deleteLines(LineTable* lt) {

    if(lt->nextLine != NULL) {
        deleteLines(lt->nextLine);
    }
    free(lt);
    lt = NULL;
}

void deleteTables(SymbolTable* st) {

    if(st->nextTable != NULL) {
        deleteTables(st->nextTable);
    }
    if(st->firstLine != NULL) {
        deleteLines(st->firstLine);
    }
    free(st);
    st = NULL;
}

int addTable(SymbolTable** st, SymbolTable** nt) {

    if(*nt == NULL) {

        return 0;
    }

    if (*st == NULL) {
        
        *st = *nt;
    
    } else {
        
        addTable(&(*st)->nextTable, nt);
    }

    return 1;
}

int addLine(LineTable** lt, LineTable** nl) {

    if (*nl == NULL) {
        
        return 0;
    }

    if (*lt == NULL) {
        
        *lt = *nl;
    
    } else {
        
        addLine(&(*lt)->nextLine, nl);
    }

    return 1;
}

char* getType(Node* tree) {

    char structname[64] = "struct ";
    
    if(tree->kind == Void) {

        return "void";
    
    } else if(tree->firstChild->kind == Simpletype) {

        return tree->firstChild->firstChild->u.identifier;
    }
        
    return strcat(structname, tree->firstChild->nextSibling->u.identifier);
}

int getVars(Node* tree, char vars[64][64], int pos) {

    if(tree == NULL) {
        return 0;
    }
    if(tree->firstChild->kind == Ident) {
        strcpy(vars[pos], tree->firstChild->u.identifier);
        return pos + 1;
    }
    strcpy(vars[pos], tree->firstChild->nextSibling->u.identifier);
    return getVars(tree->firstChild, vars, pos + 1);

}

int fillSymbolTable(SymbolTable** st, Node* tree) {

    char type[64] = "";
    char var[64] = "";
    char vars[64][64];
    int lenvars = 0;

    LineTable* lt = NULL;
    SymbolTable* nt = NULL;

    if ((*st) == NULL) {

        return 0;
    }
    if(tree == NULL) {
        
        return 1;
    }

    switch(tree->kind) {
        
        case EnTeteFonct:
            /* Déclaration de fonction */
            strcpy(var, tree->firstChild->nextSibling->u.identifier); /* récupération du nom de la fonction */
            if(!isInTable((*st)->firstLine, var)) {
                exit(2);    
            }

            strcpy(type, getType(tree->firstChild)); /* récupération du type de la fonction */
            lt = makeNewLine();
            if(lt == NULL) {

                return 0;
            }
            fillLine(lt, type, var);
            addLine(&(*st)->firstLine, &lt); /* ajout du type et du nom de la fonction à la table */

            nt = makeNewTable(var); /* création d'une nouvelle table des symbôles pour la fonction */
            if(nt == NULL) {

                return 0;
            }
            
            if(tree->firstChild->nextSibling->nextSibling->firstChild->kind == ListTypVar) {
                fillSymbolTable(&nt, tree->firstChild->nextSibling->nextSibling->firstChild); /* On remplit la nouvelle table avec les paramètres de la fonction */
            }
            fillSymbolTable(&nt, tree->nextSibling->firstChild); /* On remplit la nouvelle table avec les vatriables déclarées dans le corps de la fonction */
            addTable(st, &nt); /* On link la nouvelle table */

        case ListTypVar: 
            /* Paramètres des fonctions */
            if(strcmp((*st)->tableName, "GLOBAL")) {

                if(tree->firstChild->kind == Type) {
                    strcpy(var, tree->firstChild->nextSibling->u.identifier);
                    if(!isInTable((*st)->firstLine, var)) {
                        exit(2);    
                    }

                    strcpy(type, getType(tree->firstChild));
                    lt = makeNewLine();
                    if(lt == NULL) {

                        return 0;
                    }
                    fillLine(lt, type, var);
                    addLine(&(*st)->firstLine, &lt);
                
                } else {

                    strcpy(var, tree->firstChild->nextSibling->nextSibling->u.identifier);
                    if(!isInTable((*st)->firstLine, var)) {
                        exit(2);    
                    }

                    if(tree->firstChild->nextSibling->kind == Type) {
                        strcpy(type, getType(tree->firstChild->nextSibling));
                    }

                    lt = makeNewLine();
                    if(lt == NULL) {

                        return 0;
                    }
                    fillLine(lt, type, var);
                    addLine(&(*st)->firstLine, &lt);
                }
            }
            break;

        case DeclVars:
            if(strcmp((*st)->tableName, "GLOBAL")) {
                if(tree->firstChild != NULL) {
                    /* tree->firstChild->kind correspond à DeclVars */
                    lenvars = getVars(tree->firstChild->nextSibling->nextSibling, vars, 0); /* Récupération de la liste des variables et de la taille de cette même liste */
                    strcpy(type, getType(tree->firstChild->nextSibling)); /* Récupération du type de la liste des variables */
                    
                    for(int i = 0; i < lenvars; i++) { /* Pour chaque variable on vérifie si elle est déjà présente dans la table sinon on l'ajoute avec son type */
                        
                        if(!isInTable((*st)->firstLine, vars[i])) {
                            exit(2);    
                        }

                        lt = makeNewLine();
                        if(lt == NULL) {

                            return 0;
                        }
                        fillLine(lt, type, vars[i]);
                        addLine(&(*st)->firstLine, &lt);
                        lt = NULL;
                    }
                }
            }
            break;
        
        case TypesVars:
            if(tree->firstChild != NULL) {
                /* tree->firstChild->kind correspond à TypeVars */
                if(tree->firstChild->nextSibling->kind == Type) {
                    
                    lenvars = getVars(tree->firstChild->nextSibling->nextSibling, vars, 0); /* Récupération de la liste des variables et de la taille de cette même liste */
                    strcpy(type, getType(tree->firstChild->nextSibling)); /* Récupération du type de la liste des variables */
                    
                    for(int i = 0; i < lenvars; i++) { /* Pour chaque variable on vérifie si elle est déjà présente dans la table sinon on l'ajoute avec son type */
                        
                        if(!isInTable((*st)->firstLine, vars[i])) {
                            exit(2);    
                        }

                        lt = makeNewLine();
                        if(lt == NULL) {

                            return 0;
                        }
                        fillLine(lt, type, vars[i]);
                        addLine(&(*st)->firstLine, &lt);
                        lt = NULL;
                    }
                }
                /*printf("plouf\n");*/
            }
            break;

        default: break;
    }           

    fillSymbolTable(st, tree->nextSibling);
    fillSymbolTable(st, tree->firstChild);
    return 1;
}
