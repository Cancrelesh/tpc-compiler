
/* to_asm_file.h */
#ifndef ASM
#define ASM

typedef struct datas {

    char name[64];
    
    enum {
        
        u,
        i,
        c
    
    } type;
    
    union {

        int i;
        char c;
    
    } val;

    struct datas* next;

} Datas;

int createFileAsm(Node* tree, SymbolTable* st);
int writeAsmByReaddingTree(FILE* output, Node* tree, SymbolTable* st, Datas* datas);
int addDatasValue(Datas** datas, char* tableName, char* var, Node* value);
int insertDatas(Datas** datas, char* tableName, char* var, char* type);
void removeDatas(Datas* datas);
Datas* initDatas();
void printDatasASM(Datas* datas, FILE* output);
void printDatas(Datas* datas);

#endif